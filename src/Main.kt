fun main() {
    interface Basketball {
        fun play()
    }

    class Goldenstate : Basketball {
        override fun play() {
            println("Play Goldenstate!")
        }
    }

    class Lakers : Basketball {
        override fun play() {
            println("Play Lakers!")
        }
    }

    class BasketballFactory {
        fun create(type: String): Basketball? {
            return when (type) {
                "Goldenstate" -> Goldenstate()
                "Lakers" -> Lakers()
                else -> null
            }
        }
    }


]