interface Football {
    fun play()
}

class Barcelona : Football {
    override fun play() {
        println("Play Barcelona!")
    }
}

class Realmadrid : Football {
    override fun play() {
        println("Play Realmadrid!")
    }
}

class FootballFactory {
    fun create(type: String): Football? {
        return when (type) {
            "Barcelona" -> Barcelona()
            "Realmadrid" -> Realmadrid()
            else -> null
        }
    }
}